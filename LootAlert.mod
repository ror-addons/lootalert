<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="LootAlert" version="1.2.1" date="03/07/2017" >
		<VersionSettings gameVersion="1.4.8" windowsVersion="1.0" savedVariablesVersion="1.0" />
		<Author name="Sullemunk"/>
		<Description text="Alerts you when you recive loot, By Sullemunk for Wargrimnir on RoR" />
		<Files>
		<File name="LootAlert.lua" />
		<File name="LootAlert.xml" />
		</Files>
			<Dependencies>
			<Dependency name="LibSlash" optional="false" forceEnable="true" />
        </Dependencies>
		<OnInitialize>
			<CallFunction name="LootAlert.OnInitialize" /> 
		</OnInitialize>
		   <OnUpdate>
		<CallFunction name="LootAlert.Update" />
    	  </OnUpdate>
	</UiMod>
</ModuleFile>