LootAlert = {}
local version = "1.2.1"

function LootAlert.OnInitialize()
RegisterEventHandler(TextLogGetUpdateEventId("Chat"), "LootAlert.OnChatLogUpdated") -- runs function LootAlert.OnChatLogUpdated everytime text gets added in the chatlog
TextLogAddEntry("Chat", 0, L"<icon00057> LootAlert "..towstring(version)..L" Loaded.")
LootAlertLootTable = {}
LootAlertLootNumberIndex = 1

LootAlertStackTable = {}
LootListTimer = 0

AlertTextWindow.TypeInfo[ 101 ]     =   { font = "font_alert_outline_medium", halfFont = "font_alert_outline_half_medium", color = "Gray"   }
AlertTextWindow.TypeInfo[ 102 ]   =   { font = "font_alert_outline_medium", halfFont = "font_alert_outline_half_medium", color = "White" }
AlertTextWindow.TypeInfo[ 103 ]   =   { font = "font_alert_outline_medium", halfFont = "font_alert_outline_half_medium", color = "Teal" }
AlertTextWindow.TypeInfo[ 104 ]   =   { font = "font_alert_outline_medium", halfFont = "font_alert_outline_half_medium", color = "Blue" }
AlertTextWindow.TypeInfo[ 105 ]   =   { font = "font_alert_outline_medium", halfFont = "font_alert_outline_half_medium", color = "Purple" }
AlertTextWindow.TypeInfo[ 106 ]   =   { font = "font_alert_outline_medium", halfFont = "font_alert_outline_half_medium", color = "Gold" }

LibSlash.RegisterSlashCmd("lootlist", LootAlert.Show)
LibSlash.RegisterSlashCmd("bagcheck", function(inputname) LootStuff(inputname) end)


	
	CreateWindow ("LootAlertWindow", false)

LootAlert.Colors={}

LootAlert.Colors[1]=DefaultColor.GREY	
LootAlert.Colors[2]=DefaultColor.WHITE
LootAlert.Colors[3]=DefaultColor.GREEN
LootAlert.Colors[4]=DefaultColor.BLUE
LootAlert.Colors[5]=DefaultColor.PURPLE
LootAlert.Colors[6]=DefaultColor.GOLD
	
	LabelSetText ("LootAlertWindowName", L"<icon61>Loot List")	
	
	--WindowSetDimensions("ChatterBoxStack",275,445)
	WindowSetDimensions("LootAlertWindow",348,33+(#LootAlertLootTable*26))
	WindowSetShowing("LootAlertWindow",false)
	LootAlert.UpdateColor()
	
end

function LootAlert.Update(TimeElapsed)

--As soon as something goes into the Stack-table, a timer starts to run because of Item-To-Bag lag
	if #LootAlertStackTable > 0 then
	LootListTimer = LootListTimer + 1
	end
	
	if LootListTimer > 50 then
	LootListTimer = 0
	
	for i=1 , #LootAlertStackTable do
	
	if LootAlertStackTable[i].text ~= nil then
	LootAlert.GetLoot(LootAlertStackTable[i].text,LootAlertStackTable[i].quantity,LootAlertStackTable[i].TimeStamp)
	else
	LootAlertStackTable[i] = nil
	end
	end
	LootAlertStackTable = {}
	end
	
	
if #LootAlertLootTable <= 16 then
WindowSetDimensions("LootAlertWindow",348,33+(#LootAlertLootTable*26))
else
WindowSetDimensions("LootAlertWindow",348,33+(16*26))
end
	
end

function LootAlert.OnChatLogUpdated(updateType, filterType)
	if( updateType == SystemData.TextLogUpdate.ADDED ) then --if something gets ADDED to the chatlog...
		local TimeStamp, filterId, text = TextLogGetEntry( "Chat", TextLogGetNumEntries("Chat") - 1 ) -- ..check the latest entry in chatlog and saves the entry in "text", aslo saves the time in label TimeSTamp
		if filterId == 30 then -- checks if it's from the "loot" filter, we only want loot to show up on the alert
			local text2 = text:match(L"You receive [%a]+ ([^%.]+)") -- matches the saved entry in "text" to this specific string, if it matches it saves the loot item name in "text2"
			local quantity = text:match(L"[^%.]+([%d]).") -- strip out the numbers at the end and save it in quantity string
			if text2 ~= nil or text2~= L"" then -- if the "text2" is NOT empty, then print out the content on the Alert stack
	--		['�i]%s? ([%a]+)
	
	-- This will add the Loot to a Timer list. I had to do this because sometimes the function runs the bagcheck before the loot has gotten into the bag
	--local LootStackNumber = #LootAlertStackTable+1
			LootAlertStackTable[#LootAlertStackTable+1] = {}			
			LootAlertStackTable[#LootAlertStackTable].text = text2
			LootAlertStackTable[#LootAlertStackTable].quantity = quantity
			LootAlertStackTable[#LootAlertStackTable].TimeStamp = TimeStamp
					
		--	TempIcon,TempRarity,TempID = LootAlert.CheckLoot(text2,quantity,TimeStamp)
			
			
			end
		end
	end
end


function LootAlert.CheckLoot(CheckLootName,CheckLootQuant,TimeStamp)

if CheckLootName == nil or CheckLootName == L"" then return end

EA_Window_Backpack.UpdateBackpackSlots()


local LootInput = tostring(CheckLootName)

	if string.match(LootInput,"%-") ~= nil then 
	LootInput = string.gsub (LootInput, "%-", "") -- need to check if the item name has an "-" in it and remove it, becase the matching doesnt like using "-" in the comparison
	end
	
	if string.match(LootInput,"%s") ~= nil then 
	LootInput = string.gsub (LootInput, "%s", "") -- if the item name has an "space" in it and remove it because some item names has junk spaces after them (Blame Ryan for that!)
	end

	
local LootInput2 = string.sub(LootInput,2,-6) -- remove some extra letters at the end for easyer comparison (incase they have more junk characters)
--	d(LootInput2)

for i=1,4 do
EA_Window_Backpack.UpdateBackpackSlots()

local BagItems = EA_Window_Backpack.GetItemsFromBackpack(i) --2
	for BagSlask, SlaskBag in pairs( BagItems ) do

	
	local CheckName2 = tostring(SlaskBag.name)

	if string.match(CheckName2,"%-") ~= nil then 
	CheckName2 = string.gsub (CheckName2, "%-", "") -- need to check if the item name has an "-" in it and remove it, becase the matching doesnt like using "-" in the comparison
	end
	
	if string.match(CheckName2,"%s") ~= nil then 
	CheckName2 = string.gsub (CheckName2, "%s", "") -- need to check if the item name has an "-" in it and remove it, becase the matching doesnt like using "-" in the comparison
	end

--if string.match(tostring(CheckName2),tostring(LootInput2)) ~= nil then 

if string.match(tostring(string.lower(CheckName2)),tostring(string.lower(LootInput2))) ~= nil then

--d(CheckName2)


--d(L"Bag checkup:")
--d(L"Checking for: "..towstring(LootInput2)..L" in --"..towstring(CheckName2)..L"--")
		local CheckTempIcon = tonumber(SlaskBag.iconNum)
--d("Comparing")
LootAlertLootTable[LootAlertLootNumberIndex] = {TimeStamp=towstring(TimeStamp),Table=SlaskBag.uniqueID,Name=towstring(SlaskBag.name),ListNumber=tonumber(LootAlertLootNumberIndex),Icon=tonumber(CheckTempIcon),Rarity=tonumber(SlaskBag.rarity),ID=tonumber(SlaskBag.id),Quant=L"x"..towstring(CheckLootQuant),Colour = DataUtils.GetItemRarityColor(SlaskBag)}

LootAlertLootNumberIndex = LootAlertLootNumberIndex+1

	LootAlert.DisplayOrder = {}
	 LootAlert.Listdata = {}
	local shtcount2 = 1
	for LootAlertIndex, _ in pairs( LootAlertLootTable ) do

	table.insert(LootAlert.DisplayOrder, shtcount2)

	shtcount2 = shtcount2+1
	SortLootList(LootAlertLootTable,LootAlert.DisplayOrder)
		
	end
	LootAlert.TempLootList = LootAlertLootTable
	ListBoxSetDisplayOrder("LootAlertListBox", LootAlert.DisplayOrder)
	
	
return SlaskBag.iconNum,SlaskBag.rarity,SlaskBag.id

end
end   		
end
d("Didnt Find it")


end


function SortLootList(index,tablename)-- Sorting the tables
 
    table.sort(index, function (a,b)
     return (a.ListNumber > b.ListNumber) 
	end)

end


function LootAlert.UpdateColor()
    for row, data in pairs( LootAlertLootTable) 
    do
        if row < 17 then
        local rowName   = "LootAlertListBoxRow"..row
		local RowNumber = 	ListBoxGetDataIndex("LootAlertListBox",row)

 	 	LabelSetTextColor( rowName.."Name", LootAlertLootTable[RowNumber].Colour.r,LootAlertLootTable[RowNumber].Colour.g,LootAlertLootTable[RowNumber].Colour.b )
	
		WindowSetTintColor(  rowName.."IconFrame",LootAlertLootTable[RowNumber].Colour.r,LootAlertLootTable[RowNumber].Colour.g,LootAlertLootTable[RowNumber].Colour.b  )
		
			
		local texture, x, y, disabledTexture = GetIconData(tonumber(LootAlertLootTable[RowNumber].Icon))
		DynamicImageSetTexture (rowName.."Icon", texture, x, y)		
end
end

if #LootAlertLootTable == 0 then 
ListBoxSetVisibleRowCount("LootAlertListBox",0)
else
ListBoxSetVisibleRowCount("LootAlertListBox",16)
end



end

function LootAlert.Close()
WindowSetShowing("LootAlertWindow",false)
end

function LootAlert.Show()
WindowSetShowing("LootAlertWindow",true)
end


function LootAlert.Clicky()
--itemData = GetDatabaseItemData( itemId )

local _index = ListBoxGetDataIndex("LootAlertListBox", WindowGetId(SystemData.MouseOverWindow.name))
local TempGrabWindow = SystemData.MouseOverWindow.name
local TempGrabID = GetDatabaseItemData(LootAlertLootTable[_index].Table)

actionText = L""
textColor  = Tooltips.COLOR_ACTION
--EA_Window_Backpack.DisplayItemTooltip( TempGrabID, TempGrabWindow )

        Tooltips.CreateItemTooltip( TempGrabID, 
                                    TempGrabWindow,
                                    Tooltips.ANCHOR_WINDOW_RIGHT, 
                                    Tooltips.ENABLE_COMPARISON, 
                                    actionText, textColor, false )
									
end

--this is only for bag ItemName checkup, use: "/bagcheck <ItemName>" and the results will be printed out in debug window (no need to be exact name)
function LootStuff(inputname)

local LootInput = tostring(inputname)

	if string.match(LootInput,"%-") ~= nil then 
	LootInput = string.gsub (LootInput, "%-", "") -- need to check if the item name has an "-" in it and remove it, becase the matching doesnt like using "-" in the comparison
	end
	
	if string.match(LootInput,"%s") ~= nil then 
	LootInput = string.gsub (LootInput, "%s", "") -- need to check if the item name has an "-" in it and remove it, becase the matching doesnt like using "-" in the comparison
	end


for i=1,4 do
local BagItems2 = EA_Window_Backpack.GetItemsFromBackpack(i) --2
	for BagSlask2, SlaskBag2 in pairs( BagItems2 ) do

	
	local CheckName2 = tostring(SlaskBag2.name)

	if string.match(CheckName2,"%-") ~= nil then 
	CheckName2 = string.gsub (CheckName2, "%-", "") -- need to check if the item name has an "-" in it and remove it, becase the matching doesnt like using "-" in the comparison
	end
	
	if string.match(CheckName2,"%s") ~= nil then 
	CheckName2 = string.gsub (CheckName2, "%s", "") -- need to check if the item name has an "space" in it and remove it
	end

	
if string.match(tostring(string.lower(CheckName2)),tostring(string.lower(LootInput))) ~= nil then 

--d(CheckName2)

end



end   		
end
d("End of search")
end




function LootAlert.GetLoot(text2,quantity,TimeStamp)

			TempIcon,TempRarity,TempID = LootAlert.CheckLoot(text2,quantity,TimeStamp)

			if TempIcon ~= nil then
			local icon = TempIcon
			
		if icon < 10000 and icon > 1000 then
		TempIcon = L"icon0"..towstring(icon)
		elseif icon < 1000  and icon > 100 then
		TempIcon = L"icon00"..towstring(icon)
		elseif icon < 100	then
		TempIcon = L"icon000"..towstring(icon)
		else 
		TempIcon = L"icon"..towstring(icon)
		end		
		else
		TempIcon = L"icon00057"
		TempRarity = 2
		end		
			
			local TempTypeNumber = 100+tonumber(TempRarity)		--font color 
				SystemData.AlertText.VecType = {TempTypeNumber} --font type
				SystemData.AlertText.VecText = {L"<"..towstring(TempIcon)..L"> "..towstring(text2)} --text to be printed
				AlertTextWindow.AddAlert() --fire off the alert ticker...
				PlaySound(GameData.Sound.AUCTION_HOUSE_CREATE_AUCTION) -- ...and play a nice sound
			

		return

end